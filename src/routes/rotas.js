const router = require("express").Router();
const dbController = require("../controller/dbController");
const ClienteController = require("../controller/clienteController");
const MesaController = require("../controller/mesaController");


router.get("/tables", dbController.getTableNames);
router.get("/tables/descriptions", dbController.getTableDescriptions);

router.post("/cadastro", ClienteController.criarCliente);
router.get("/clientes", ClienteController.getCliente);
router.put("/editar", ClienteController.editCliente);
router.delete("/delete/:id", ClienteController.deletarCliente);

router.get("/mesa", MesaController.getMesa)
router.post("/cadastroMesa", MesaController.criarMesa);
router.put("/editarMesa", MesaController.editMesa);
router.delete("/deletarMesa/:id", MesaController.deletarMesa);

router.post("/login",ClienteController.login);

module.exports = router;
