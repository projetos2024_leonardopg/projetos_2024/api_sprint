const express = require('express')
const app = express();
const cors = require('cors');
const testConnect = require('./db/testConnect')

class AppController {
  constructor() {
    this.express = express();
    this.middlewares();
    this.routes();
    this.testConnect = testConnect();

  }

  middlewares() {
    this.express.use(express.json());
    this.express.use(cors());
  }

  routes() {
    const apiRoutes = require('./routes/rotas');
    this.express.use('/restaurante/', apiRoutes);
  }
}

module.exports = new AppController().express;