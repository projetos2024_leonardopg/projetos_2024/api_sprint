const connect = require("../db/connect");

module.exports = class ClienteController {

    // Método assíncrono para criar um novo cliente
    static async criarCliente(req, res) {
        // Extrair dados do corpo da requisição
        const { nome, email, telefone, cpf, senha } = req.body;

        // Verificar se todos os campos obrigatórios foram preenchidos
        if (nome && email && telefone && cpf && senha) {
            // Query para inserir um novo cliente no banco de dados
            const query = `INSERT INTO cliente (nome, email, telefone, cpf, senha) VALUES (
                '${nome}', 
                '${email}', 
                '${telefone}', 
                '${cpf}',
                '${senha}'
            )`;

            try {
                // Executar a query no banco de dados
                connect.query(query, function (err) {
                    if (err) {
                        // Lidar com erros, se houver
                        console.log(err); // Registrar o erro no console
                        res.status(500).json({ error: "Erro ao cadastrar cliente no banco de dados" }); // Responder com status de erro em JSON
                        return;
                    }
                    console.log("Cliente cadastrado com sucesso!"); // Registrar sucesso no console
                    res.status(200).json({ message: "Cadastrado com sucesso!" }); // Responder com status de sucesso em JSON
                });
            } catch (error) {
                // Lidar com erros caso ocorra uma exceção
                console.error("Erro ao executar o insert", error); // Registrar o erro no console
                res.status(500).json({ error: "Erro interno do servidor" }); // Responder com status de erro em JSON
            }
        } else {
            // Responder com status 400 e mensagem de erro em JSON caso algum campo esteja faltando
            res.status(400).json({ message: "Por favor, preencha todos os campos." });
        }
    }

    // Método assíncrono para obter informações de todos os clientes
    static async getCliente(req, res) {
        try {
            // Query para selecionar todos os clientes
            const query = `SELECT * FROM cliente`;
            // Executar a query no banco de dados
            connect.query(query, function (err, result) {
                if (err) {
                    // Lidar com erros, se houver
                    console.log(err); // Registrar o erro no console
                    res.status(500).json({ error: "Erro ao obter cliente do banco de dados" }); // Responder com status de erro em JSON
                    return;
                }
                console.log("Cliente obtido com sucesso"); // Registrar sucesso no console
                res.status(200).json(result); // Responder com os dados do cliente em JSON
            });
        } catch (error) {
            // Lidar com erros caso ocorra uma exceção
            console.error("Erro ao obter cliente:", error); // Registrar o erro no console
            res.status(500).json({ error: "Erro interno do servidor" }); // Responder com status de erro em JSON
        }
    }

    // Método assíncrono para editar as informações de um cliente existente
    static async editCliente(req, res) {
        // Extrair dados do corpo da requisição
        const { nome, telefone, email, cpf, senha } = req.body;
        try {
            // Verificar se todos os campos obrigatórios foram preenchidos
            if (nome && telefone && email && cpf && senha) {
                // Query para atualizar as informações do cliente no banco de dados
                const query = `UPDATE cliente SET nome = '${nome}', telefone = '${telefone}', email = '${email}', '${senha}' WHERE cpf = '${cpf}'`;
                // Executar a query no banco de dados
                connect.query(query, function (err) {
                    if (err) {
                        // Lidar com erros, se houver
                        console.log(err); // Registrar o erro no console
                        res.status(500).json({ error: "Erro ao editar cliente no banco de dados" }); // Responder com status de erro em JSON
                        return;
                    }
                    console.log("Cliente editado com sucesso"); // Registrar sucesso no console
                    res.status(200).json({ message: "Editado com sucesso" }); // Responder com status de sucesso em JSON
                });
            } else {
                // Responder com status 400 e mensagem de erro em JSON caso algum campo esteja faltando
                res.status(400).json({ message: "Por favor, preencha todos os campos." });
            }
        } catch (error) {
            // Lidar com erros caso ocorra uma exceção
            console.error("Erro ao editar cliente:", error); // Registrar o erro no console
            res.status(500).json({ error: "Erro interno do servidor" }); // Responder com status de erro em JSON
        }
    }

    // Método assíncrono para excluir um cliente existente
    static async deletarCliente(req, res) {
        // Verificar se foi fornecido um CPF para identificar o cliente a ser excluído
        if (req.params.cpf) {
            // Query para excluir o cliente com base no CPF fornecido
            const query = `DELETE FROM cliente WHERE cpf = ${req.params.cpf}`;
            try {
                // Executar a query no banco de dados
                connect.query(query, function (err) {
                    if (err) {
                        // Lidar com erros, se houver
                        console.log(err); // Registrar o erro no console
                        res.status(500).json({ error: "Erro ao excluir cliente do banco de dados" }); // Responder com status de erro em JSON
                        return;
                    }
                    console.log("Cliente excluído com sucesso"); // Registrar sucesso no console
                    res.status(200).json({ message: "Cliente excluído" }); // Responder com status de sucesso em JSON
                });
            } catch (error) {
                // Lidar com erros caso ocorra uma exceção
                console.error("Erro ao excluir cliente:", error); // Registrar o erro no console
                res.status(500).json({ error: "Erro interno do servidor" }); // Responder com status de erro em JSON
            }
        } else {
            // Responder com status 400 e mensagem de erro em JSON caso o CPF não seja fornecido
            res.status(400).json({ message: "Cliente não encontrado" });
        }
    }
};
