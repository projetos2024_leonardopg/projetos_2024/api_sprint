module.exports = class MesaController {

    // Método para criar uma nova mesa
    static async criarMesa(req, res) {
        // Extrair informações do corpo da requisição
        const { numero, capacidade, status } = req.body;

        // Verificar se todas as informações necessárias foram fornecidas
        if (numero && capacidade && status) {
            // Se sim, registrar os dados recebidos
            console.log(numero); // Registrando o numero da mesa
            console.log(capacidade); // Registrando a capacidade da mesa
            console.log(status); // Registrando o status da mesa
            // Enviar uma resposta indicando sucesso
            return res.status(200).json({ message: "Mesa cadastrada com sucesso!" });
        } else {
            // Caso contrário, informar que campos estão faltando
            return res.status(400).json({ message: "Por favor, preencha todos os campos." });
        }
    }

    // Método para obter informações de uma mesa existente
    static async getMesa(req, res) {
        try {
            // Simulando dados de uma mesa existente
            const mesa = {
                numero: 1,
                capacidade: 4,
                status: 'Disponível'
            };
            // Responder com os dados da mesa
            res.status(200).json(mesa);
        } catch (error) {
            // Lidar com erros caso ocorram
            console.error("Erro ao obter mesa:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    // Método para editar informações de uma mesa existente
    static async editMesa(req, res) {
        const { numero, capacidade, status } = req.body;
        try {
            // Verificar se todas as informações necessárias foram fornecidas
            if (numero && capacidade && status) {
                // Se sim, responder com mensagem de sucesso
                res.status(200).json({ message: "Mesa editada com sucesso" });
            } else {
                // Caso contrário, informar que campos estão faltando
                res.status(400).json({ message: "Por favor, preencha todos os campos." });
            }
        } catch (error) {
            // Lidar com erros caso ocorram
            console.error("Erro ao editar mesa:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    // Método para excluir uma mesa existente
    static async deletarMesa(req, res) {
        // Verificar se foi fornecido um número para a mesa a ser excluída
        if (req.params.id) {
            // Se sim, responder com mensagem de sucesso
            res.status(200).json({ message: "Mesa excluída" });
        } else {
            // Caso contrário, informar que a mesa não foi encontrada
            res.status(400).json({ message: "Mesa não encontrada" });
        }
    }
};
