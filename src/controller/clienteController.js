module.exports = class ClienteController {

    // Método para criar um novo cliente
    static async criarCliente(req, res) {
        // Extrair informações do corpo da requisição
        const { nome, email, telefone, cpf, senha } = req.body;

        // Verificar se todas as informações necessárias foram fornecidas
        if (nome && email && telefone && cpf && senha) {
            // Se sim, registrar os dados recebidos
            console.log(nome); // Registrando o nome do cliente
            console.log(email); // Registrando o email do cliente
            console.log(telefone); // Registrando o telefone do cliente
            console.log(cpf); // Registrando o CPF do cliente
            console.log(senha); // Registrando a senha do cliente
            // Enviar uma resposta indicando sucesso
            return res.status(200).json({ message: "Cadastrado com sucesso!" });
        } else {
            // Caso contrário, informar que campos estão faltando
            return res.status(400).json({ message: "Por favor, preencha todos os campos." });
        }
    }

    // Método para obter informações de um cliente existente
    static async getCliente(req, res) {
        try {
            // Simulando dados de um cliente existente
            const cliente = {
                nome: 'Gabriel',
                cpf: '12345678910',
                email: 'leo@gmail.com',
                telefone: '123456789',
                senha: '12345'
            };
            // Responder com os dados do cliente
            res.status(200).json(cliente);
        } catch (error) {
            // Lidar com erros caso ocorram
            console.error("Erro ao obter cliente:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    // Método para editar informações de um cliente existente
    static async editCliente(req, res) {
        const { nome, telefone, email, cpf, senha } = req.body;
        try {
            // Verificar se todas as informações necessárias foram fornecidas
            if (nome && telefone && email && cpf && senha) {
                // Se sim, responder com mensagem de sucesso
                res.status(200).json({ message: "Editado com sucesso" });
            } else {
                // Caso contrário, informar que campos estão faltando
                res.status(400).json({ message: "Por favor, preencha todos os campos." });
            }
        } catch (error) {
            // Lidar com erros caso ocorram
            console.error("Erro ao editar cliente:", error);
            res.status(500).json({ error: "Erro interno do servidor" });
        }
    }

    // Método para excluir um cliente existente
    static async deletarCliente(req, res) {
        // Verificar se foi fornecido um ID para o cliente a ser excluído
        if (req.params.id) {
            // Se sim, responder com mensagem de sucesso
            res.status(200).json({ message: "Cliente Excluido" });
        } else {
            // Caso contrário, informar que o cliente não foi encontrado
            res.status(400).json({ message: "Cliente não encontrado" });
        }
    }

    static async login(req, res) {
        // Extrair informações do corpo da requisição
        const { cpf, senha } = req.body;

        // Verificar se o e-mail e a senha foram fornecidos
        if (cpf && senha) {
            // Aqui você pode adicionar a lógica de autenticação
            // Exemplo simples: verificar se o e-mail e a senha correspondem a um usuário válido
            if (cpf === '12345678910' && senha === '123') {
                // Se as credenciais estiverem corretas, retornar um token de autenticação
                // Aqui você pode gerar um token JWT, por exemplo
                const token = 'seu-token-de-autenticacao';
                // Responder com o token de autenticação
                return res.status(200).json({ token });
            } else {
                // Se as credenciais estiverem incorretas, responder com uma mensagem de erro
                return res.status(401).json({ message: "Credenciais inválidas" });
            }
        } else {
            // Se o e-mail ou a senha não foram fornecidos, responder com uma mensagem de erro
            return res.status(400).json({ message: "Por favor, forneça e-mail e senha" });
        }
    }
};
